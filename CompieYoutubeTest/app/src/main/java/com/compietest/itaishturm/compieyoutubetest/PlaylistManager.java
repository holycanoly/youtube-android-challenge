package com.compietest.itaishturm.compieyoutubetest;

import java.util.List;


public class PlaylistManager {
    public static List<Playlist> GetPlaylists()
    {
        String jsonoutput = MainFactory.getMainFactory().getJasonJoader().loadJSONFromAsset("YoutubeList.json");
        List<Playlist> fromJSON = MainFactory.getMainFactory().getJasonJoader().getFromJSON(jsonoutput);

        return fromJSON;
    }

}
