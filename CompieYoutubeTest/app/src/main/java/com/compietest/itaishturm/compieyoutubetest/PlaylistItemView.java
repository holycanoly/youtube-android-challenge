package com.compietest.itaishturm.compieyoutubetest;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;


public class PlaylistItemView extends View {


    public PlaylistItemView(Context context) {
        super(context);
        init(null, 0);
    }

    public PlaylistItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }


    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.PlaylistItemView, defStyle, 0);

    }
}
