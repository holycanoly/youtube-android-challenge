package com.compietest.itaishturm.compieyoutubetest;

/**
 * Created by ItaiShturm on 1/29/2017.
 */

public class PlaylistItem {
    public String Title;
    public String Link;
    public String Thumb;
}
