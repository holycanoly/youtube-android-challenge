package com.compietest.itaishturm.compieyoutubetest;

import android.app.Activity;

/**
 * Created by ItaiShturm on 1/29/2017.
 */

public class MainFactory {
    // temp for handling all managment
    private static MainFactory main;
    private Activity activity;
    private JSONLoader jsl = new JSONLoader();

    private MainFactory()
    {}
    public static MainFactory getMainFactory()
    {
        if(main==null)
            main = new MainFactory();
        return main;
    }

    public JSONLoader getJasonJoader()
    {
        return jsl;
    }

    public void setMainActivity(Activity activity){
        this.activity = activity;
    }

    // no exception handling - time :/
    public Activity getMainActivity()
    {
        return  activity;
    }
}
