package com.compietest.itaishturm.compieyoutubetest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ItaiShturm on 1/29/2017.
 */

public class Playlist {

    public String Title;
    public List<PlaylistItem> playlistItems = new ArrayList<PlaylistItem>();

    public void AddplayList(PlaylistItem Playlist)
    {playlistItems.add(Playlist);}
}
