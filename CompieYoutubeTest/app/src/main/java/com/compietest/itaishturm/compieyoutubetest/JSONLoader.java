package com.compietest.itaishturm.compieyoutubetest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ItaiShturm on 1/29/2017.
 */

public class JSONLoader {
    public JSONLoader(){};
    public List<Playlist> getFromJSON(String JsonString)
    {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(JsonString));
            JSONArray m_jArry = obj.getJSONArray("Playlists");
            List<Playlist> playlists = new ArrayList<>();

            // foreach playlist
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject currPlaylistJson = m_jArry.getJSONObject(i);
                Playlist curplaylist = new Playlist();
                curplaylist.Title = currPlaylistJson.getString("ListTitle");
                JSONArray listItems = currPlaylistJson.getJSONArray("ListItems");
                // foreach listitem
                for (int j=0; j< listItems.length(); j++)
                {
                    JSONObject curPlayItemJson = listItems.getJSONObject(j);
                    PlaylistItem curPlaylistItem = new PlaylistItem();
                    curPlaylistItem.Title = curPlayItemJson.getString("Title");
                    curPlaylistItem.Link = curPlayItemJson.getString("link");
                    curPlaylistItem.Thumb = curPlayItemJson.getString("thumb");
                    curplaylist.AddplayList(curPlaylistItem);
                }
                playlists.add(curplaylist);
            }
            return  playlists;
        } catch (org.json.JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String loadJSONFromAsset(String JsonFileName) {//"yourfilename.json"
        String json = null;
        try {
            InputStream is = MainFactory.getMainFactory().getMainActivity().getAssets().open(JsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
