package com.compietest.itaishturm.compieyoutubetest;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.List;

// - Notes
// - This is a WIP, as one, it is not final
// - currently the main logic is:
// - Getting Data -main done - from file, should update to a proper get from server, missing Testing+logging
// - Main data structure - done(naked code)
// - UI structure - in progress, mainly missing Object init
// - UI logic - missing buttons connections+ action handlers
// - ui - beauty - n/a
// - youTube - api handling - done
//
public class YoutubeApp_MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_app__main);
        mRecyclerView = (RecyclerView) findViewById(R.id.MainTubeRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<Playlist> playlists = PlaylistManager.GetPlaylists();

        // the adapter is responsible for the UI basic hadling for the RecycleView
        mAdapter = new RecycleViewAdapter(playlists);
        mRecyclerView.setAdapter(mAdapter);
    }



    // get the video link from the entire address
    private String youTubeLinkGet(String linkAddress)
    {
        StringBuilder sb = new StringBuilder(linkAddress);
        String linkOnly =  sb.substring(sb.indexOf("="));
        String linkWithoutBraclets = sb.substring(1,linkOnly.length()-1);
        return linkWithoutBraclets;
    }

    public void uponClickOnYoutubeVideo(String YouTubeVideoId)
    {
        final String YouTubeVideo = YouTubeVideoId;
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(YouTubeVideo);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        // I'm using here one of my own security keys for youtube-google certification,
        // it will work for a while(I will remove this from my account in a week or so)
        youTubePlayerView.initialize("AIzaSyCH95GRFRAMkDy1guMSWXhkF_YrsdVlU6U", onInitializedListener);
    }
}
